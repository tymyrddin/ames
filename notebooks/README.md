# Ames Housing R&D ML pipeline

- Exploratory Data Analysis (EDA)
- Feature Engineering
- Feature Selection
- Model Training
- Obtaining Predictions / Scoring
- Feature engineering with Open Source