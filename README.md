# Ames Housing

A training wheel: Deployment of a continuous regression model for the Ames Housing dataset.

* [Building the model in a ML pipeline](notebooks)
* [Packaging the model using Tox](production-model-package)
* [Exposing model using FastAPI](model-serving-api)
* [CI and publishing](ci-and-publishing)
* [Deploying ML API with containers](deploying-with-containers)
* [Differential testing in CI](differential-testing)
* [Deploying to Iaas](iaas)

## Resources

### Feature Engineering

- [Feature Engineering for Machine Learning](https://www.udemy.com/course/feature-engineering-for-machine-learning/?referralCode=A855148E05283015CF06) - Online Course
- [Packt Feature Engineering Cookbook](https://www.packtpub.com/data/python-feature-engineering-cookbook) - Book
- [Feature Engineering for Machine Learning: A comprehensive Overview](https://trainindata.medium.com/feature-engineering-for-machine-learning-a-comprehensive-overview-a7ad04c896f8) - Article
- [Practical Code Implementations of Feature Engineering for Machine Learning with Python](https://towardsdatascience.com/practical-code-implementations-of-feature-engineering-for-machine-learning-with-python-f13b953d4bcd) - Article
- [Best Resources to Learn about Feature Engineering for Machine Learning](https://trainindata.medium.com/best-resources-to-learn-feature-engineering-for-machine-learning-6b4af690bae7)
- [Practical Code Implementation of Feature Engineering Techniques with Python](https://towardsdatascience.com/practical-code-implementations-of-feature-engineering-for-machine-learning-with-python-f13b953d4bcd)

### Feature Selection

- [Feature Selection for Machine Learning](https://www.udemy.com/course/feature-selection-for-machine-learning/?referralCode=186501DF5D93F48C4F71) - Online Course
- [Feature Selection for Machine Learning: A comprehensive Overview](https://trainindata.medium.com/feature-selection-for-machine-learning-a-comprehensive-overview-bd571db5dd2d) - Article

### Machine Learning

- [Best Resources to Learn Machine Learning](https://trainindata.medium.com/find-out-the-best-resources-to-learn-machine-learning-cd560beec2b7) - Article
- [Machine Learning with Imbalanced Data](https://www.udemy.com/course/machine-learning-with-imbalanced-data/?referralCode=F30537642DA57D19ED83) - Online Course

### Scikit-Learn and sklearn pipeline

- [Introduction to Scikit-Learn](https://www.oreilly.com/ideas/intro-to-scikit-learn)
- [Six reasons why I recommend Scikit-Learn](https://www.oreilly.com/ideas/six-reasons-why-i-recommend-scikit-learn)
- [Why you should learn Scikit-Learn](https://hub.packtpub.com/learn-scikit-learn/)
- [Deep dive into SKlearn pipelines](https://www.kaggle.com/baghern/a-deep-dive-into-sklearn-pipelines) -Kaggle
- [SKlearn pipeline tutorial](https://www.kaggle.com/sermakarevich/sklearn-pipelines-tutorial) - Kaggle
- [Managing Machine Learning workflows with Sklearn pipelines](https://www.kdnuggets.com/2017/12/managing-machine-learning-workflows-scikit-learn-pipelines-part-1.html)
- [A simple example of pipeline in Machine Learning using SKlearn](https://towardsdatascience.com/a-simple-example-of-pipeline-in-machine-learning-with-scikit-learn-e726ffbb6976)

### Python

- [The Hitchhickers guide to python](https://docs.python-guide.org/writing/style/)
- [FullStack Pycharm](https://www.fullstackpython.com/pycharm.html)